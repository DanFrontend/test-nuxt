export const state = () => ({
  allData: [],
  teasers: []
})

export const getters = {
  getAllData(state) {
    return state.allData
  },
  getTeasers(state) {
    return state.teasers
  }
}

export const mutations = {
  addAllData(state, payload) {
    state.allData.push(payload)
  },
  setTeasers(state, payload) {
    state.teasers = payload
  }
}

export const actions = {
  async getTeasers({ commit, state }) {
    this.$axios.get("/api/catalog/marketing/bd")
    .then(({data})=> {
      commit('setTeasers', data.list)
      commit('addAllData', {teasers: state.teasers.slice(0,2)})
    });
    
  }
}